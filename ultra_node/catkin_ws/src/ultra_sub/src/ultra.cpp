#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>
#include <iostream>

sensor_msgs::LaserScan dist_1;
double temp [5][5], out[5],lin[1][5],temps;

using namespace std;
double sort_med(float a[5][5]) //not using the function. Implemented sorting in callback
{
    float temps;
    for(int i =0;i<5;i++)
    {
        for(int j = 0;j<5;j++)
        {
            if(a[j][i]>a[j+1][i])
            {
                temps = a[j+1][i];
                a[j+1][i] = a[j][i];
                a[j][i] = temps;
            }
        }


    }

}

void scanCallback1(const sensor_msgs::LaserScan::ConstPtr& scan_1)
{	
	dist_1=*scan_1;
    for(int i =0;i<5;i++)
    {
        for(int j = 0;j<5;j++)
        {
            temp[i][j] = dist_1.ranges[j];
            //ROS_INFO("Range [%d] = [%f] \n", j,temp[i][j]);
       }

    }

    for(int i =0;i<5;i++)
    {
        for(int j = 0;j<5;j++)
        {

                if(temp[i][j]<0)
                {
                    if(i!=0)
                    {
                        temp[i][j] = temp[i-1][j];
                    }
                    else
                        temp[i][j] = 25;
                }

                if(temp[j][i]>temp[j+1][i])
                {
                    temps = temp[j+1][i];
                    temp[j+1][i] = temp[j][i];
                    temp[j][i] = temps;
                }
          }

    out[i] = temp[3][i];
    ROS_INFO("Range %d \t = [%f] \n",i,out[i]);
    }
//ROS_INFO("Range = [%f] \n", temp[1][2]);
//ROS_INFO("Range2 = [%f] \n", temp[1][2]);
//dist_1.ranges[4]
}

int main (int argc, char **argv)
{ 	
	ros::init(argc, argv,"ultra_listener");
    ros::NodeHandle np;
    ros::Subscriber topic1;
    topic1 = np.subscribe<sensor_msgs::LaserScan>("/guidance/obstacle_distance", 100, scanCallback1);
    ros::spin();

    ros::Publisher scan_pub = np.advertise<sensor_msgs::LaserScan>("filtered_ultra", 100);

    int count = 0;
    double intensities[5];
    ros::Rate loop_rate(10);


    while(np.ok())
    {
        for(int i=0;i<5;++i)
        {
            intensities[i] = 0;
        }
        ros::Time scan_time = ros::Time::now();
        sensor_msgs::LaserScan output;
        output.header.stamp = scan_time;
        output.header.frame_id = "laser_frame";
        output.angle_min = -1.57;
        output.angle_max = 1.57;
        output.angle_increment = 3.14;
        output.time_increment = 0;
        output.range_min = 0.0;
        output.range_max = 100.0;
        output.ranges.resize(5);
        output.intensities.resize(5);
        for(unsigned int i = 0; i < 5; ++i)
           {
              output.ranges[i] = out[i];
              output.intensities[i] = intensities[i];
           }

        scan_pub.publish(output);
        ros::spinOnce();
        loop_rate.sleep();
        ++count;

    }

    return 0;

}
